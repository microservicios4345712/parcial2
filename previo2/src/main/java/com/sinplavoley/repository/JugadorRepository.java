package com.sinplavoley.repository;

import com.sinplavoley.model.Jugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface JugadorRepository extends JpaRepository<Jugador, Long> {
    List<Jugador> findByEstadoAfiliacion(String estadoAfiliacion);
}
