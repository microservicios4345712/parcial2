package com.sinplavoley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinplavoleyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SinplavoleyApplication.class, args);
    }
}
